import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { IHeroCharacter } from '../../../interfaces/hero.interface';

@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.scss']
})
export class HeroFormComponent implements OnInit {
  @Output() createNewHero = new EventEmitter<IHeroCharacter>();

  heroForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.heroForm = this.formBuilder.group({
      heroName: ['', [Validators.required]],
      picture: ['']
    });
  }

  ngOnInit() {}

  submitForm(value: IHeroCharacter) {
    this.createNewHero.emit(value);
  }
}
