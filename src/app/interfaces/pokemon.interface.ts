export interface IPokemon {
  name: string;
  id: number;
  image: string;
  types: string[];
}

export interface ISpritesApiResponse {
  front_default: string;
}

export interface ITypesNameApiResponse {
  name: string;
}

export interface ITypesApiResponse {
  type: ITypesNameApiResponse;
}

export interface IPokemonApiResponse {
  name: string;
  id: number;
  sprites: ISpritesApiResponse;
  types: ITypesApiResponse[];
}